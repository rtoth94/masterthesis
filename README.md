# MasterThesis

Applications of Subsurface Scattering Techniques for Dynamic Rendering of 3D Range Scans of Translucent Objects

Application: https://drive.google.com/file/d/1HECNMwmAvYTpaEWbO2Vq4lRouiEsUBpC/view?usp=sharing

Demo: https://www.youtube.com/watch?v=Gci80De2nf0

NVIDIA Support only

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Gci80De2nf0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

